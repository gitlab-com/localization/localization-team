# Localization Team

[Globalization & Localization Team](https://about.gitlab.com/handbook/marketing/localization/) uses this project for:

- Issues and discussions related to team-specific work 
- Product management of localization-specific initiatives 
- Globalization engineering work 
- Globalization enablement and program management
- Non-transactonal and operational discussions with our [LSP (localization service and technology provider) partners](https://gitlab.com/groups/gitlab-com/localization/-/epics/60), like [Spartan Software, Inc.](https://gitlab.com/gitlab-com/localization/localization-team/-/issues/41) and [Argos Multilingual](https://gitlab.com/gitlab-com/localization/localization-team/-/issues/60) 
